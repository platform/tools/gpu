# Get full path to the directory containing this script.  $0 isn't valid
# when this script is sourced from another script or the command-line.
PROGDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Top-level directory from which to run go commands.  This is currently the
# same as $GOPATH.
GPU_BUILD_ROOT=`cd $PROGDIR/../../../../../../ && pwd`

# The root of the Android Studio repo.
REPO_ROOT=`cd $PROGDIR/../../../../../../../../ && pwd`

# The path to the gpu source code (relative to $GPU_BUILD_ROOT/src).
GPU_RELATIVE_SOURCE_PATH=android.googlesource.com/platform/tools/gpu

# Add the tools we build to the path.
export PATH=$GPU_BUILD_ROOT/bin:$PATH
