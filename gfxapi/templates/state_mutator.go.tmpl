{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{Global "module" ""}}
{{Include "go_common.tmpl"}}
{{$ | Macro "state_mutator.go" | GoFmt | Write "state_mutator.go"}}

{{define "state_mutator.go"}}
  {{template "Go.GeneratedHeader" (Global "OutputDir")}}

  import (
    "fmt"

    "android.googlesource.com/platform/tools/gpu/atom"
    "android.googlesource.com/platform/tools/gpu/binary"
    "android.googlesource.com/platform/tools/gpu/database"
    "android.googlesource.com/platform/tools/gpu/gfxapi"
    "android.googlesource.com/platform/tools/gpu/log"
    "android.googlesource.com/platform/tools/gpu/memory"
  )

  func getState(s *gfxapi.State) *State {
    api := API()
    if state, ok := s.APIs[api].(*State); ok {
      return state
    } else {
      if s.APIs == nil {
        s.APIs = make(map[gfxapi.API]binary.Object)
      }
      state = &State{}
      state.Init()
      s.APIs[api] = state
      return state
    }
  }
¶
  {{range $c := AllCommands $}}
    {{if not (GetAnnotation $c "no_mutate")}}
      func (ϟa *{{Template "Go.CmdName" $c}}) Mutate(ϟs *gfxapi.State, ϟd database.Database, ϟl log.Logger) error {
        ϟc := getState(ϟs)
        _ = ϟc
        {{Template "Go.CommandLogic" $c}}
        return nil
      }
    {{end}}
  {{end}}

{{end}}
