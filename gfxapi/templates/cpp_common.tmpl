{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{Include "common.tmpl"}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ type for the specified AST type.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Type"}}
  {{if not (IsType $)}}{{Template "C++.Type" (TypeOf $)}}
  {{else if IsVoid        $}}void
  {{else if IsBool        $}}bool
  {{else if IsInt         $}}int
  {{else if IsUint        $}}unsigned int
  {{else if IsChar        $}}char
  {{else if IsU8          $}}uint8_t
  {{else if IsS8          $}}int8_t
  {{else if IsU16         $}}uint16_t
  {{else if IsS16         $}}int16_t
  {{else if IsF32         $}}float
  {{else if IsU32         $}}uint32_t
  {{else if IsS32         $}}int32_t
  {{else if IsF64         $}}double
  {{else if IsU64         $}}uint64_t
  {{else if IsS64         $}}int64_t
  {{else if IsEnum        $}}{{Template "C++.EnumType" $}}
  {{else if IsReference   $}}std::shared_ptr<{{Template "C++.Type" $.To}}>
  {{else if IsString      $}}std::string
  {{else if IsStaticArray $}}{{Template "C++.Type" $.ValueType}}
  {{else if IsSlice       $}}Slice<{{if IsVoid $.To}}uint8_t{{else}}{{Template "C++.Type" $.To}}{{end}}>
  {{else if IsPointer     $}}{{Template "C++.Type" $.To}}*
  {{else if IsAny         $}}auto
  {{else}}{{Template "C++.TypeName" $}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the fixed-size-array postfix for static array types.
-------------------------------------------------------------------------------
*/}}
{{define "C++.StaticArraySuffix"}}
  {{if not (IsType $)}}{{Template "C++.StaticArraySuffix" (TypeOf $)}}
  {{else if IsStaticArray $}}[{{$.Size}}]§
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a typename that can be used for string concatenation for the given type.
-------------------------------------------------------------------------------
*/}}
{{define "C++.TypeName"}}
  {{if not (IsType $)}}{{Template "C++.TypeName" (TypeOf $)}}
  {{else if IsPseudonym   $}}{{$.Name}}
  {{else if IsClass       $}}{{Global "prefix"}}{{$.Name}}
  {{else if IsEnum        $}}{{Global "prefix"}}{{$.Name}}
  {{else if IsMap         $}}{{Template "C++.TypeName" $.KeyType}}To{{Template "C++.TypeName" $.ValueType}}
  {{else if IsVoid        $}}Void
  {{else if IsBool        $}}Bool
  {{else if IsInt         $}}Int
  {{else if IsUint        $}}Uint
  {{else if IsChar        $}}Char
  {{else if IsU8          $}}U8
  {{else if IsS8          $}}S8
  {{else if IsU16         $}}U16
  {{else if IsS16         $}}S16
  {{else if IsF32         $}}F32
  {{else if IsU32         $}}U32
  {{else if IsS32         $}}S32
  {{else if IsF64         $}}F64
  {{else if IsU64         $}}U64
  {{else if IsS64         $}}S64
  {{else if IsString      $}}String
  {{else if IsReference   $}}{{Template "C++.TypeName" $.To}}__R
  {{else if IsSlice       $}}{{Template "C++.TypeName" $.To}}__S
  {{else if IsPointer     $}}{{Template "C++.TypeName" $.To}}__{{if $.Const}}C{{end}}P
  {{else}}{{Error "C++.TypeName passed unsupported type (%T): %s" $ $.Name}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ default value for the provided AST type.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Null"}}
  {{AssertType $ "Type"}}
  {{     if IsBool       $}}false
  {{else if IsInt        $}}0
  {{else if IsUint       $}}0
  {{else if IsS8         $}}0
  {{else if IsU8         $}}0
  {{else if IsS16        $}}0
  {{else if IsU16        $}}0
  {{else if IsS32        $}}0
  {{else if IsU32        $}}0
  {{else if IsF32        $}}0
  {{else if IsS64        $}}0
  {{else if IsU64        $}}0
  {{else if IsF64        $}}0
  {{else if IsEnum       $}}0
  {{else if IsString     $}}""
  {{else if IsPseudonym  $}}{{Template "C++.Null" $.To}}
  {{else if IsClass      $}}{{Template "C++.Type" $}}()
  {{else if IsSlice      $}}{{Template "C++.Type" $}}()
  {{else if IsMap        $}}{{Template "C++.Type" $}}()
  {{else if IsPointer    $}}nullptr
  {{else if IsReference  $}}{{Template "C++.Type" $}}()
  {{else}}{{Error "macro Null called with unsupported type: %s" $.Name}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ type for the specified Enum.
  By default this emits the enum name, but this behaviour can be overridden with
  the global "C++.EnumTypeOverride".
-------------------------------------------------------------------------------
*/}}
{{define "C++.EnumType"}}
  {{AssertType $ "Enum"}}

  {{if $override := Global "C++.EnumTypeOverride"}}{{$override}}
  {{else}}{{Template "C++.EnumName" $}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the comma-separated list of type-name paired parameters for given
  function.
-------------------------------------------------------------------------------
*/}}
{{define "C++.CallParameters"}}
  {{AssertType $ "Function"}}

  {{range $i, $p := $.CallParameters}}
    {{if $i}}, {{end}}{{Template "C++.ParameterType" $p}} {{$p.Name}}§
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the comma-separated list of function parameter types without the
  parameter names.
-------------------------------------------------------------------------------
*/}}
{{define "C++.CallParameterTypes"}}
  {{AssertType $ "Function"}}

  {{range $i, $p := $.CallParameters}}
    {{if $i}}, {{end}}{{Template "C++.ParameterType" $p}}§
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the comma-separated list of argument names for the given function.
-------------------------------------------------------------------------------
*/}}
{{define "C++.CallArguments"}}
  {{AssertType $ "Function"}}

  {{range $i, $p := $.CallParameters}}
    {{if $i}}, {{end}}{{$p.Name}}§
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ type for use as a parameter for the given parameter or type.
-------------------------------------------------------------------------------
*/}}
{{define "C++.ParameterType"}}
  {{AssertType $ "Type" "Parameter"}}

  {{     if IsParameter $}}{{Template "C++.ParameterType" (TypeOf $)}}
  {{else if IsPseudonym $}}{{Template "C++.ParameterType" $.To}}
  {{else if IsPointer   $}}{{Template "C++.ParameterType" $.To}}*
  {{else if IsString    $}}char*
  {{else if IsAny       $}}{{Template "C++.ParameterTypeAny" $}}
  {{else                 }}{{Template "C++.Type" $}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  If the specified parameter is of type 'any' then this macro emits the
  corresponding template parameter name. If the parameter is not of type 'any'
  then nothing is emitted.
-------------------------------------------------------------------------------
*/}}
{{define "C++.ParameterTypeAny"}}
  {{AssertType $ "Parameter"}}

  {{if IsAny (TypeOf $)}}
    {{$index := IndexOf $.Function.FullParameters $}}
    ARG{{$index}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ return type for the given command.
-------------------------------------------------------------------------------
*/}}
{{define "C++.ReturnType"}}
  {{AssertType $ "Function"}}

  {{Template "C++.ParameterType" $.Return}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic for all the statements in the given block.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Block"}}
  {{range $s := $.Statements}}
    {{$v := Macro "C++.Statement" $s}}
    {{$v}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the name for the given local variable.
-------------------------------------------------------------------------------
*/}}
{{define "C++.LocalName"}}
  {{AssertType $ "Local"}}

  l_{{$.Name}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to execute the given statement.
  If the global "C++.Statement.Override" is specified then this the macro with
  the specified name is called, otherwise the macro delegates to
  "C++.Statement.Default".
-------------------------------------------------------------------------------
*/}}
{{define "C++.Statement"}}
  {{if $override := Global "C++.Statement.Override"}}
    {{Template $override $}}
  {{else}}
    {{Template "C++.Statement.Default" $}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to execute the given statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Statement.Default"}}
  {{     if IsDeclareLocal $}}{{Template "C++.Type" (TypeOf $.Local)}} {{Template "C++.LocalName" $.Local}} = {{Template "C++.Read" $.Local.Value}};
  {{else if IsAssign       $}}{{Template "C++.Assign" $}}
  {{else if IsArrayAssign  $}}{{Template "C++.ArrayAssign" $}}
  {{else if IsMapAssign    $}}{{Template "C++.MapAssign" $}}
  {{else if IsSliceAssign  $}}{{Template "C++.SliceAssign" $}}
  {{else if IsReturn       $}}return {{Template "C++.Read" $.Value}};
  {{else if IsBranch       $}}{{Template "C++.Branch" $}}
  {{else if IsSwitch       $}}{{Template "C++.Switch" $}}
  {{else if IsIteration    $}}{{Template "C++.Iteration" $}}
  {{else if IsCall         $}}{{Template "C++.Call" $}};
  {{else if IsAssert       $}}
  {{else if IsRead         $}}read({{Template "C++.Read" $.Slice}});
  {{else if IsWrite        $}}write({{Template "C++.Read" $.Slice}});
  {{else if IsCopy         $}}copy({{Template "C++.Read" $.Dst}}, {{Template "C++.Read" $.Src}});
  {{else if IsFence        $}}{{if not (IsNil $.Statement)}}{{Template "C++.Statement" $.Statement}}{{end}}
  {{else                    }}{{Error "unsupported statement %T: %+v" $ $}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to perform an assign statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Assign"}}
  {{AssertType $ "Assign"}}

  {{if IsIgnore $.LHS}}
    {{if ne $.Operator "="}}{{Error "Compound assignments to '_' are not supported (%s)" $.Operator}}{{end}}
    {{Template "C++.Read" $.RHS}};
  {{else}}
    {{Template "C++.Read" $.LHS}} {{$.Operator}} {{Template "C++.Read" $.RHS}};
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to perform a static-array assign statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.ArrayAssign"}}
  {{AssertType $ "ArrayAssign"}}

  {{Template "C++.Read" $.To.Array}}[{{Template "C++.Read" $.To.Index}}] {{$.Operator}} {{Template "C++.Read" $.Value}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to perform a map assign statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.MapAssign"}}
  {{AssertType $ "MapAssign"}}

  {{Template "C++.Read" $.To.Map}}[{{Template "C++.Read" $.To.Index}}] {{$.Operator}} {{Template "C++.Read" $.Value}};
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to perform a slice assign statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.SliceAssign"}}
  {{AssertType $ "SliceAssign"}}

  {{if eq $.Operator "="}}write({{Template "C++.Read" $.To.Slice}}, {{Template "C++.Read" $.To.Index}}, {{Template "C++.Read" $.Value}});
  {{else}}{{Error "Unsupported MapAssign operator %s" $.Operator}}{{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic for a branch statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Branch"}}
  {{AssertType $ "Branch"}}

  if ({{Template "C++.Read" $.Condition}}) {
    {{Template "C++.Block" $.True}}
    {{if len $.False.Statements}}
  } else {
    {{Template "C++.Block" $.False}}
    {{end}}
  }
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic for a switch statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Switch"}}
  {{AssertType $ "Switch"}}

  switch ({{Template "C++.Read" $.Value}}) {
    {{range $i, $c := $.Cases}}
      {{range $j, $cond := $c.Conditions}}
        case {{Template "C++.Read" $cond}}:{{if HasMore $j $c.Conditions}} // fall-through...{{else}} {{"{"}}{{end}}
      {{end}}
        {{Template "C++.Block" $c.Block}}
        break;
      }
    {{end}}
  }
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to perform an iteration statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Iteration"}}
  {{AssertType $ "Iteration"}}

  {{if IsBinaryOp $.Iterable}}
    {{Template "C++.Range" "Name" (Macro "C++.Read" $.Iterator) "Range" $.Iterable}} {
      {{Template "C++.Block" $.Block}}
    }
  {{else}}
    {{$it := Macro "C++.LocalName" $.Iterator}}
    for ({{Template "C++.Type" $.Iterable}}::iterator {{$it}} = {{Template "C++.Read" $.Iterable}}; {{$it}} != {{Template "C++.Read" $.Iterable}}.end(); ++{{$it}}) {
      {{Template "C++.Block" $.Block}}
    }
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to read the given expression.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Read"}}
  {{     if IsBoolValue        $}}{{$}}
  {{else if IsNumericValue     $}}{{$}}
  {{else if IsStringValue      $}}{{printf "%q" $}}
  {{else if IsParameter        $}}{{$.Name}}
  {{else if IsBinaryOp         $}}{{Template "C++.Read" $.LHS}} {{$.Operator}} {{Template "C++.Read" $.RHS}}
  {{else if IsUnaryOp          $}}{{$.Operator}}({{Template "C++.Read" $.Expression}})
  {{else if IsEnumEntry        $}}{{Template "C++.EnumName" $.Owner}}::{{Template "C++.EnumEntryName" $}}
  {{else if IsLocal            $}}{{Template "C++.LocalName" $}}
  {{else if IsBitTest          $}}({{Template "C++.Read" $.Bitfield}} & {{Template "C++.Read" $.Bits}}) != 0
  {{else if IsBinaryOp         $}}({{Template "C++.Read" $.LHS}}) {{$.Operator}} ({{Template "C++.Read" $.RHS}})
  {{else if IsCast             $}}{{Template "C++.Cast" $}}
  {{else if IsCall             $}}{{Template "C++.Call" $}}
  {{else if IsUnknown          $}}{{Global "C++.InferredExpression" "1"}}{{Template "C++.Read" $.Inferred}}{{Global "C++.InferredExpression" ""}}
  {{else if IsObserved         $}}{{Template "C++.Read" $.Parameter}}
  {{else if IsMember           $}}{{Template "C++.Dereference" $.Object}}m{{$.Field.Name}}
  {{else if IsGlobal           $}}this->{{$.Name}}
  {{else if IsMapIndex         $}}{{Template "C++.Read" $.Map}}[{{Template "C++.Read" $.Index}}]
  {{else if IsMapContains      $}}{{Template "C++.Read" $.Map}}.count({{Template "C++.Read" $.Key}}) > 0
  {{else if IsLength           $}}{{Template "C++.Type" $.Type}}(({{Template "C++.ReadLength" "Type" (TypeOf $.Object) "Value" $.Object}}))
  {{else if IsNull             $}}{{Template "C++.Null" $.Type}}
  {{else if IsNew              $}}{{$ty := Unpack $.Type}}std::shared_ptr<{{Template "C++.Type" $ty.To}}>(new {{Template "C++.Type" $ty.To}}())
  {{else if IsCreate           $}}{{$ty := Unpack $.Type}}std::shared_ptr<{{Template "C++.Type" $ty.To}}>((new {{Template "C++.Type" $ty.To}}()){{Template "C++.InitializeFields" $.Initializer}})
  {{else if IsArrayInitializer $}}{{Template "C++.Type" $}}{ {{ForEach $.Values "C++.Read" | JoinWith ", "}} }
  {{else if IsClassInitializer $}}{{Template "C++.Type" $}}(){{Template "C++.InitializeFields" $}}
  {{else if IsSliceIndex       $}}{{Template "C++.SliceIndex" $}}
  {{else if IsSliceRange       $}}slice({{Template "C++.Read" $.Slice}}, {{Template "C++.Read" $.Range.LHS}}, {{Template "C++.Read" $.Range.RHS}})
  {{else if IsPointerRange     $}}slice({{Template "C++.Read" $.Pointer}}, {{Template "C++.Read" $.Range.LHS}}, {{Template "C++.Read" $.Range.RHS}})
  {{else if IsClone            $}}clone({{Template "C++.Read" $.Slice}})
  {{else if IsMake             $}}make<{{Template "C++.Type" ($.Type | Unpack).To}}>({{Template "C++.Read" $.Size}})
  {{else if IsSelect           $}}{{Template "C++.Select" $}}
  {{else}}{{Error "macro C++.Read called with unsupported type: %T" $}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a chain of 'fluent' set calls, initializing the target class.
-------------------------------------------------------------------------------
*/}}
{{define "C++.InitializeFields"}}
  {{AssertType $ "ClassInitializer"}}
  {{range $f := $.Fields}}.Set{{$f.Field.Name}}({{Template "C++.Read" $f.Value}}){{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a C++ dereference (-> or .) for the given value.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Dereference"}}
  {{Template "C++.Read" $}}{{if or (IsReference (TypeOf $))}}->{{else}}.{{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a comma-separated list of reads.
-------------------------------------------------------------------------------
*/}}
{{define "C++.ReadList"}}
  {{range $i, $v := $}}
    {{if $i}}, {{end}}{{Template "C++.Read" $v}}§
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to execute an iteration statement.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Range"}}
  {{AssertType $.Name "string"}}
  {{AssertType $.Range "BinaryOp"}}

  for ({{Template "C++.Type" $.Range.RHS}} {{$.Name}} = {{Template "C++.Read" $.Range.LHS}}; {{$.Name}} < {{Template "C++.Read" $.Range.RHS}}; ++{{$.Name}})
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to execute a cast expression.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Cast"}}
  {{AssertType $ "Cast"}}

  {{$src_ty := $.Object | TypeOf | Underlying | Unpack}}
  {{$dst_ty := $.Type | Underlying | Unpack}}
  {{$src    := Macro "C++.Read" $.Object}}

  {{/* A[] -> B[] */}}{{if and (IsSlice $src_ty) (IsSlice $dst_ty)}}
    {{$src}}.as<{{Template "C++.Type" $dst_ty.To}}>()
  {{/* T[] -> T* */}}{{else if and (IsSlice $src_ty) (IsPointer $dst_ty)}}
    {{$src}}.begin()
  {{/* char* -> string */}}{{else if and (IsPointer $src_ty) (IsString $dst_ty)}}
    string({{$src}})
  {{/* char[] -> string */}}{{else if and (IsSlice $src_ty) (IsString $dst_ty)}}
    string({{$src}})
  {{/* string[] -> char[] */}}{{else if and (IsString $src_ty) (IsSlice $dst_ty)}}
    slice({{$src}})
  {{else}}
    ({{Template "C++.Type" $.Type}})({{$src}})
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the logic to read the length of the specified expression by
  Value and Type.
-------------------------------------------------------------------------------
*/}}
{{define "C++.ReadLength"}}
  {{AssertType $.Type "Type"}}

  {{$ty := Unpack $.Type}}
  {{     if IsPseudonym   $ty}}{{Template "C++.ReadLength" "Type" $ty.To "Value" $.Value}}
  {{else if IsSlice       $ty}}{{Template "C++.Read" $.Value}}.count()
  {{else if IsString      $ty}}{{Template "C++.Read" $.Value}}.size()
  {{else if IsMap         $ty}}{{Template "C++.Read" $.Value}}.size()
  {{else                     }}{{Error "macro C++.ReadLength called with unsupported type: %v" $.Type}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to execute a call expression.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Call"}}
  {{AssertType $ "Call"}}

  {{if $.Target.Object}}{{Template "C++.MethodCall" $}}
  {{else}}{{/* extern */}}
    {{$.Target.Function.Name}}({{Template "C++.ReadList" $.Arguments}})
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to execute a slice index operation.
-------------------------------------------------------------------------------
*/}}
{{define "C++.SliceIndex"}}
  {{AssertType $ "SliceIndex"}}

  {{if Global "C++.InferredExpression"}}
    {{Template "C++.Read" $.Slice}}[{{Template "C++.Read" $.Index}}]
  {{else}}
    read({{Template "C++.Read" $.Slice}}, {{Template "C++.Read" $.Index}})
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic for a select expression.
-------------------------------------------------------------------------------
*/}}
{{define "C++.Select"}}
  {{AssertType $ "Select"}}

  /* clang-format off */
  /* switch({{Template "C++.Read" $.Value}}) */»
  {{range $c := $.Choices}}
    /* case {{range $i, $cond := $c.Conditions}}{{if $i}}, {{end}}{{Template "C++.Read" $cond}}{{end}}: */§
    ({{range $i, $cond := $c.Conditions}}§
        {{if $i}} || {{end}}(({{Template "C++.Read" $.Value}}) == ({{Template "C++.Read" $cond}}))§
    {{end}}) ? ({{Template "C++.Read" $c.Expression}}) :
  {{end}}
  /* default: */ {{Template "C++.Null" $.Type}}« /* clang-format on */
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ logic to execute a method call expression.
-------------------------------------------------------------------------------
*/}}
{{define "C++.MethodCall"}}
  {{$is_map_delete := and (IsMap (TypeOf $.Target.Object)) (eq $.Target.Function.Name "Delete")}}

  {{if $is_map_delete}}{{Template "C++.Read" $.Target.Object}}.erase({{Template "C++.ReadList" $.Arguments}})
  {{else             }}{{Template "C++.Read" $.Target.Object}}.{{$.Target.Function.Name}}({{Template "C++.ReadList" $.Arguments}})
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Transforms the given string or identifier into a format used for a public
  type or field. Example:
    this_is_public -> ThisIsPublic
-------------------------------------------------------------------------------
*/}}
{{define "C++.Public"}}
  {{AssertType $ "string"}}

  {{$ | SplitOn "_" | Title}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a comment stating that the file is automatically generated.
-------------------------------------------------------------------------------
*/}}
{{define "C++.GeneratedHeader"}}⋖{{Copyright "generated" "apic"}}⋗{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the Android Open Source Project copyright header.
-------------------------------------------------------------------------------
*/}}
{{define "C++.AOSP.Copyright"}}⋖{{Copyright "generated_aosp_c" "apic"}}⋗{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a comment block containing the specified text.
-------------------------------------------------------------------------------
*/}}
{{define "C++.CommentHeader"}}
  {{AssertType $ "string"}} {{/* The comment block body text */}}
  ////////////////////////////////////////////////////////////////////////////////
  // {{.}}
  ////////////////////////////////////////////////////////////////////////////////
{{end}}

{{$clang_style := "{BasedOnStyle: Google, AccessModifierOffset: -4, ColumnLimit: 100, ContinuationIndentWidth: 8, IndentWidth: 4}"}}
{{Global "clang-format" (Strings "clang-format" "-style" $clang_style)}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ name of an enum type.
-------------------------------------------------------------------------------
*/}}
{{define "C++.EnumName"}}
  {{AssertType $ "Enum"}}

  {{if $p := Global "prefix"}}{{$p}}{{Title $.Name}}
  {{else}}{{$.Name}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ name of an enum entry.
-------------------------------------------------------------------------------
*/}}
{{define "C++.EnumEntryName"}}
  {{AssertType $.Owner "Enum"}}
  {{AssertType $.Name "string"}}

  {{/* TODO: Get rid of enum prefixes. */}}
  {{if $enumPrefix := GetAnnotation $.Owner "prefix"}}
    {{index $enumPrefix.Arguments 0}}_§
  {{else if $apiPrefix := Global "prefix"}}
    {{Upper $apiPrefix}}_§
    {{$.Owner.Name | SplitPascalCase | Upper | JoinWith "_"}}_§
  {{end}}
  {{TrimLeft "_" $.Name}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the printf format code for the specified type
-------------------------------------------------------------------------------
*/}}
{{define "C++.PrintfFormatCode"}}
  {{if not (IsType $)}}{{Template "C++.PrintfFormatCode" (TypeOf $)}}
  {{else if IsPseudonym     $}}{{Template "C++.PrintfFormatCode" $.To}}
  {{else if IsPointer       $}}%p
  {{else if IsClass         $}}%p
  {{else if IsString        $}}%s
  {{else if IsBool          $}}%d
  {{else if IsU8            $}}%"PRIu8"
  {{else if IsS8            $}}%"PRId8"
  {{else if IsU16           $}}%"PRIu16"
  {{else if IsS16           $}}%"PRId16"
  {{else if IsF32           $}}%f
  {{else if IsU32           $}}%"PRIu32"
  {{else if IsS32           $}}%"PRId32"
  {{else if IsF64           $}}%f
  {{else if IsU64           $}}%"PRIu64"
  {{else if IsS64           $}}%"PRId64"
  {{else if IsInt           $}}%d
  {{else if IsUint          $}}%u
  {{else if IsEnum          $}}%u
  {{else}}{{Error "C++.PrintfFormatCode passed unsupported type: %s" $.Name}}
  {{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits a printf-style string and arguments for logging a call to command.
-------------------------------------------------------------------------------
*/}}
{{define "C++.PrintfCommandCall"}}
  {{AssertType $ "Function"}}

  {{$name := Macro "CmdName" $}}
  {{$args := Macro "C++.CallArguments" $}}

  "{{$name}}({{ForEach $.CallParameters "C++.PrintfFormatCode" | JoinWith ", "}})\n"
  {{if $args}}, {{$args}}{{end}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the typedef function-pointer name for a given command.
-------------------------------------------------------------------------------
*/}}
{{define "C++.FunctionPtrType"}}
  {{AssertType $ "Function"}}

  PFN{{Macro "CmdName" $ | Upper}}
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ function-pointer typedef for a given command.
-------------------------------------------------------------------------------
*/}}
{{define "C++.TypedefFunctionPtr"}}
  {{AssertType $ "Function"}}

  typedef {{Template "C++.ReturnType" $}} (STDCALL *{{Template "C++.FunctionPtrType" $}})({{Template "C++.CallParameters" $}});
{{end}}


{{/*
-------------------------------------------------------------------------------
  Emits the C++ function-pointer variable extern for a given command.
-------------------------------------------------------------------------------
*/}}
{{define "C++.FunctionPtrDecl"}}
  {{AssertType $ "Function"}}

  {{Template "C++.FunctionPtrType" $}} {{Template "CmdName" $}}
{{end}}
