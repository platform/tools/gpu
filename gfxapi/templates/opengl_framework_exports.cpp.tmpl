{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}

{{Include "opengl_framework_exports.tmpl"}}
{{Include "cpp_common.tmpl"}}
{{$ | Macro "opengl_framework_exports.cpp" | Reflow 4 | Write "opengl_framework_exports.cpp"}}

{{/*
-------------------------------------------------------------------------------
  Entry point.
-------------------------------------------------------------------------------
*/}}
{{define "opengl_framework_exports.cpp"}}
  {{/* This is the list of functions that the OpenGL framework needs to export. */}}
  {{$opengl_framework_exports := Strings (Macro "OpenGLFrameworkExports") | SplitOn "\n"}}

  {{/* This is the list of functions that the spy exports. */}}
  {{$api_exports := (ForEach (AllCommands $) "CmdName")}}

  {{/* This is list of functions we need to expose. */}}
  {{$expose := $opengl_framework_exports | FilterOut $api_exports}}

  {{Template "C++.AOSP.Copyright"}}
¶
#include <gapic/get_gfx_proc_address.h>
#include <gapic/target.h>
¶
extern "C" {«
¶
  {{range $f := $expose}}
    static void* real__{{$f}} = nullptr;
    EXPORT void __attribute__((naked)) {{$f}}() {
      asm("jmp *%%rax" : : "a"(real__{{$f}}) : );
    }
  {{end}}
¶
  __attribute__((constructor))
  static void initExports() {
    {{range $f := $expose}}
      real__{{$f}} = gapic::GetGfxProcAddress("{{$f}}", true);
    {{end}}
  }
»} // extern "C"
¶
{{end}}
