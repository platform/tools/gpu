// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glClientWaitSync.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glClientWaitSync.xhtml","OpenGL ES 3.1")
cmd GLenum glClientWaitSync(GLsync sync, GLbitfield syncFlags, GLuint64 timeout) {
  minRequiredVersion(3, 0)
  supportsBits(syncFlags, GL_SYNC_FLUSH_COMMANDS_BIT)
  if (GL_SYNC_FLUSH_COMMANDS_BIT in syncFlags) {
  }

  return ?
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glDeleteSync.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glDeleteSync.xhtml","OpenGL ES 3.1")
cmd void glDeleteSync(GLsync sync) {
  minRequiredVersion(3, 0)

}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glFenceSync.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glFenceSync.xhtml","OpenGL ES 3.1")
cmd GLsync glFenceSync(GLenum condition, GLbitfield syncFlags) {
  minRequiredVersion(3, 0)
  switch (condition) {
    case GL_SYNC_GPU_COMMANDS_COMPLETE: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(condition)
    }
  }

  return ?
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glGetSynciv.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glGetSynciv.xhtml","OpenGL ES 3.1")
cmd void glGetSynciv(GLsync sync, GLenum pname, GLsizei bufSize, GLsizei* length, GLint* values) {
  minRequiredVersion(3, 0)
  switch (pname) {
    case GL_OBJECT_TYPE, GL_SYNC_CONDITION, GL_SYNC_FLAGS, GL_SYNC_STATUS: {
      // version 3.0
    }
    default: {
      glErrorInvalidEnum(pname)
    }
  }
  // TODO
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glIsSync.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glIsSync.xhtml","OpenGL ES 3.1")
cmd bool glIsSync(GLsync sync) {
  minRequiredVersion(3, 0)
  // TODO
  return ?
}

@Doc("https://www.khronos.org/opengles/sdk/docs/man3/html/glWaitSync.xhtml","OpenGL ES 3.0")
@Doc("https://www.khronos.org/opengles/sdk/docs/man31/html/glWaitSync.xhtml","OpenGL ES 3.1")
cmd void glWaitSync(GLsync sync, GLbitfield syncFlags, GLuint64 timeout) {
  minRequiredVersion(3, 0)

  assert(timeout == 0xFFFFFFFFFFFFFFFF) // GL_TIMEOUT_IGNORED
}

