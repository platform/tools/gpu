// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package maker

import (
	"log"
	"os"
	"path/filepath"
)

var (
	// Config holds the current configuration of the maker system.
	Config struct {
		// Verbose enables increased logging output.
		Verbose int
		// DisableParallel turns of all parallel build support.
		DisableParallel bool
		// StopOnError makes the system quit faster once an error has been found.
		StopOnError bool
	}
	// Paths holds the set of path roots for the build.
	Paths struct {
		// The root path of the build
		Root string
		// The dependancy cache directory
		Deps string
		// The application binary directory.
		Bin string
	}
	//GoPath is the GOPATH environment setting
	GoPath []string
	goTool Entity
)

func init() {
	GoPath = filepath.SplitList(os.Getenv("GOPATH"))
	if len(GoPath) == 0 {
		log.Fatalf("GOPATH %q not valid", os.Getenv("GOPATH"))
	}
	for i := range GoPath {
		GoPath[i] = CommonPath(GoPath[i])
	}
	root := GoPath[0]
	Paths.Root = root
	Paths.Deps = Path(root, "deps")
	Paths.Bin = Path(root, "bin")
	EnvVars["PATH"] = []string{Paths.Bin}
	goTool = FindTool("go")
}
