// Copyright (C) 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package value

import (
	"math"

	"android.googlesource.com/platform/tools/gpu/replay/protocol"
)

// Bool is a Value of type TypeBool.
type Bool bool

// Get returns TypeBool and 1 if the Bool is true, otherwise 0.
func (v Bool) Get(PointerResolver) (protocol.Type, uint64) {
	if v {
		return protocol.TypeBool, 1
	} else {
		return protocol.TypeBool, 0
	}
}

// U8 is a Value of type TypeUint8.
type U8 uint8

// Get returns TypeUint8 and the value zero-extended to a uint64.
func (v U8) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeUint8, uint64(v)
}

// S8 is a Value of type TypeInt8.
type S8 int8

// Get returns TypeInt8 and the value sign-extended to a uint64.
func (v S8) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeInt8, uint64(v)
}

// U16 is a Value of type TypeUint16.
type U16 uint16

// Get returns TypeUint16 and the value zero-extended to a uint64.
func (v U16) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeUint16, uint64(v)
}

// S16 is a Value of type TypeInt16.
type S16 int16

// Get returns TypeInt16 and the value sign-extended to a uint64.
func (v S16) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeInt16, uint64(v)
}

// F32 is a Value of type TypeFloat.
type F32 float32

// Get returns TypeFloat and the IEEE 754 representation of the value packed
// into the low part of a uint64.
func (v F32) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeFloat, uint64(math.Float32bits(float32(v)))
}

// U32 is a Value of type TypeUint32.
type U32 uint32

// Get returns TypeUint32 and the value zero-extended to a uint64.
func (v U32) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeUint32, uint64(v)
}

// S32 is a Value of type TypeInt32.
type S32 int32

// Get returns TypeInt32 and the value sign-extended to a uint64.
func (v S32) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeInt32, uint64(v)
}

// F64 is a Value of type TypeDouble.
type F64 float64

// Get returns TypeDouble and the IEEE 754 representation of the value packed
// into a uint64.
func (v F64) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeDouble, math.Float64bits(float64(v))
}

// U64 is a Value of type TypeUint64.
type U64 uint64

// Get returns TypeUint64 the value zero-extended to a uint64.
func (v U64) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeUint64, uint64(v)
}

// S64 is a Value of type TypeInt64.
type S64 int64

// Get returns TypeInt64 and the value reinterpreted as a uint64.
func (v S64) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeInt64, uint64(v)
}

// AbsolutePointer is a pointer in the absolute address-space that will not be
// altered before being passed to the protocol.
type AbsolutePointer uint64

// Get returns TypeAbsolutePointer and the uint64 value of the absolute pointer.
func (p AbsolutePointer) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeAbsolutePointer, uint64(p)
}

// Offset returns the sum of the pointer with offset.
func (p AbsolutePointer) Offset(offset uint64) Pointer {
	return p + AbsolutePointer(offset)
}

// IsValid returns true for all absolute pointers.
func (p AbsolutePointer) IsValid() bool { return true }

// RemappedPointer is a pointer that was observed at capture time.
// Pointers of this type are remapped to an equivalent volatile address-space
// pointer, or absolute address-space pointer before being passed to the
// protocol.
type RemappedPointer uint64

// Get returns the pointer type and the pointer translated to either an
// equivalent volatile address-space pointer or absolute pointer.
func (p RemappedPointer) Get(r PointerResolver) (protocol.Type, uint64) {
	return r.TranslateRemappedPointer(uint64(p))
}

// Offset returns the sum of the pointer with offset.
func (p RemappedPointer) Offset(offset uint64) Pointer {
	return p + RemappedPointer(offset)
}

// IsValid returns true if the pointer considered valid. Currently this is a
// test for the pointer being greater than 0x1000 as low addresses are likely
// to be a wrong interpretation of the value. This may change in the future.
func (p RemappedPointer) IsValid() bool {
	// Anything very low in applciation address-space is extremely
	// unlikely to be a valid pointer.
	return p > 0x1000
}

// VolatilePointer is a pointer to the volatile address-space.
// Unlike RemappedPointer, there is no remapping.
type VolatilePointer uint64

// Get returns TypeVolatilePointer and the uint64 value of the pointer in
// volatile address-space.
func (p VolatilePointer) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeVolatilePointer, uint64(p)
}

// Offset returns the sum of the pointer with offset.
func (p VolatilePointer) Offset(offset uint64) Pointer {
	return p + VolatilePointer(offset)
}

// IsValid returns true.
func (p VolatilePointer) IsValid() bool { return true }

// VolatileTemporaryPointer is a pointer to in temporary address-space.
// The temporary address-space sits within a reserved area of the the volatile
// address space and its offset is calculated dynamically.
type VolatileTemporaryPointer uint64

// Get returns TypeVolatilePointer and the dynamically calculated offset of the
// temporary pointer within volatile address-space.
func (p VolatileTemporaryPointer) Get(r PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeVolatilePointer, r.TranslateTemporaryPointer(uint64(p))
}

// Offset returns the sum of the pointer with offset.
func (p VolatileTemporaryPointer) Offset(offset uint64) Pointer {
	return p + VolatileTemporaryPointer(offset)
}

// IsValid returns true.
func (p VolatileTemporaryPointer) IsValid() bool { return true }

// ConstantPointer is a pointer in the constant address-space that will not be
// altered before being passed to the protocol.
type ConstantPointer uint64

// Get returns TypeConstantPointer and the uint64 value of the pointer in constant address-space.
func (p ConstantPointer) Get(PointerResolver) (protocol.Type, uint64) {
	return protocol.TypeConstantPointer, uint64(p)
}

// Offset returns the sum of the pointer with offset.
func (p ConstantPointer) Offset(offset uint64) Pointer {
	return p + ConstantPointer(offset)
}

// IsValid returns true.
func (p ConstantPointer) IsValid() bool { return true }
